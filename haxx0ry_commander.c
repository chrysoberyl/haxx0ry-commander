#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define DEBUG 0

#define NUM_STARS 90
#define MAX_LINE_LENGTH 512

#define MAX_NUM_COMMANDS 256
#define COMMAND_LENGTH 63

#define MAX_FLIGHTS 1024
#define MAX_LINKS 1024


/* Our starry stars */
typedef struct starstruct {
    int id;
    int x;
    int y;
    struct starstruct *neighbours[NUM_STARS];
    int neighbour_distances[NUM_STARS];
    int num_neighbours;
    int richness;
    int owner;
    int ship_count;
    int turns_to_prod;
    int threat;
    int impending;
    int links;
    int idle_turns;
    int ongoing_attack;
    int bastion;
} Star;


/* Our ongoing flights */
typedef struct flightstruct {
    int from;
    int to;
    int ship_count;
    int owner;
    int turns;
} Flight;


/* Probably not needed, but OK */
typedef struct linkstruct {
    int low_id;
    int high_id;
} Link;


/* Allocate and initialize good star */
Star *new_star(int id, int x, int y) {
    Star *star = malloc(sizeof(Star));
    star->id = id;
    star->x = x;
    star->y = y;
    star->neighbours[0] = 0;
    star->num_neighbours = 0;
    star->richness = 1;
    star-> owner = -2; // -1 is already "nobody"
    star-> ship_count = 5;
    star-> turns_to_prod = 5;
    star->threat = 0;
    star-> impending = 0;
    star-> links = 0;
    star->idle_turns = 0;
    star->ongoing_attack = 0;
    star->bastion = 0;
    return star;
}


/* Get euclidean distance as float, no matter how far */
float get_float_distance(Star *a, Star *b) {
    return sqrtf((float)( (a->x - b->x)*(a->x - b->x) + (a->y - b->y)*(a->y - b->y) ));
}


/* Get euclidean distance, then return our in-world flight distance.
   Return -1 to signify invalid if it was too far (>60) */
int get_flight_distance(Star *a, Star *b) {
    float fdist = sqrtf((float)( (a->x - b->x)*(a->x - b->x) + (a->y - b->y)*(a->y - b->y) ));
    if(fdist > 60.0f) {
        return -1;
    }
    else {
        return (int) ceil(fdist/10.0f);
    }
}


/* Just debug stuff */
void print_star(Star *star) {
    fprintf(stderr, "Star %d, [%d:%d]\n", star->id, star->x, star->y);
    fprintf(stderr, "Neighbours: ");
    for(int i=0; i<star->num_neighbours; i++) {
        fprintf(stderr, "%d->[%d:%d], ", star->neighbours[i]->id, star->neighbours[i]->x, star->neighbours[i]->y);
    }
    fprintf(stderr, "\n");
}


/* Conveniently add neighbours in the initial step */
void add_neighbour(Star *star, Star *neighbour, int distance) {
    star->neighbours[star->num_neighbours] = neighbour;
    star->neighbour_distances[star->num_neighbours] = distance;
    star->num_neighbours++;
}


/* Conveniently update star stats */
void update_star(Star *star, int richness, int owner, int ship_count, int turns_to_prod, int ongoing_attack) {
    star->richness = richness;
    star->owner = owner;
    star->ship_count = ship_count;
    star->turns_to_prod = turns_to_prod;
    star->ongoing_attack = ongoing_attack;
}


/* Monster */
int main(int argc, char *argv[]) {
    setvbuf(stdout, NULL, _IONBF, 0);
    char *line = malloc(MAX_LINE_LENGTH);
    sprintf(line, "%s", "");
    size_t buflen;
    time_t t;
    srand((unsigned) time(&t));

    Star *stars[NUM_STARS];

    // Get starmap
    if(getline(&line, &buflen, stdin) <= 0) {
        if(DEBUG) {
            fprintf(stderr, "Error: read ded\n");
        }
        exit(1);
    }

    // Validate...starmap? :P
    if(!strstr(line, "stars")) {
        if(DEBUG) {
            fprintf(stderr, "Error: missing stars\n");
        }
        exit(1);
    }

    // Do initial starstuff
    strtok(line, " ");
    for(int i=0; i<NUM_STARS; i++) {
        int x = atoi(strtok(NULL, " "));
        int y = atoi(strtok(NULL, " "));
        stars[i] = new_star(i, x, y);
        for(int j=0; j<i; j++) {
            int distance = get_flight_distance(stars[j], stars[i]);
            if(distance != -1) {
                add_neighbour(stars[i], stars[j], distance);
                add_neighbour(stars[j], stars[i], distance);
            }
        }
    }

    if(DEBUG) {
        for(int i=0; i<NUM_STARS; i++) {
            print_star(stars[i]);
        }
    }

    // Reduce initialization waste a bit
    Star *owned[NUM_STARS];
    Star *unconquered[NUM_STARS];
    Star *enemies[NUM_STARS];
    Star *friends[NUM_STARS];
    Flight *flights[MAX_FLIGHTS];
    Link *links[MAX_LINKS];
    char *commands[MAX_NUM_COMMANDS];

    int num_flights = 0;
    int num_links = 0;

    for(int i=0; i<MAX_NUM_COMMANDS; i++) {
        commands[i] = malloc(COMMAND_LENGTH);
    }

    for(int i=0; i<MAX_FLIGHTS; i++) {
        flights[i] = malloc(sizeof(Flight));
    }

    for(int i=0; i<MAX_LINKS; i++) {
        links[i] = malloc(sizeof(Link));
    }

    int turns = 0;

    // On each turn...
    while(1) {
        if(getline(&line, &buflen, stdin) <= 0) {
            if(DEBUG) {
                fprintf(stderr, "Error: read ded\n");
            }
            exit(1);
        }
        if(DEBUG) {
            fprintf(stderr, line);
        }

        if(strstr(line, "star")) {
            strtok(line, " ");
            int star_id = atoi(strtok(NULL, " "));
            int richness = atoi(strtok(NULL, " "));
            int owner = atoi(strtok(NULL, " "));
            int ship_count = atoi(strtok(NULL, " "));
            int turns_to_prod = atoi(strtok(NULL, " "));
            if(DEBUG) {
                fprintf(stderr, "Updating star %d with (%d, %d, %d, %d)\n",
                        star_id, richness, owner, ship_count, turns_to_prod);
            }
            update_star(stars[star_id], richness, owner, ship_count, turns_to_prod, 0);
        }
        else if(strstr(line, "link")) {
            strtok(line, " ");
            int low_id = atoi(strtok(NULL, " "));
            int high_id = atoi(strtok(NULL, " "));
            links[num_links]->low_id = low_id;
            links[num_links]->high_id = high_id;
            stars[low_id]->links += 1;
            stars[high_id]->links += 1;
            num_links++;
        }
        else if(strstr(line, "flight")) {
            if(DEBUG) {
                fprintf(stderr, "Ok, learning about flights\n");
            }
            strtok(line, " ");
            int from = atoi(strtok(NULL, " "));
            int to = atoi(strtok(NULL, " "));
            int ship_count = atoi(strtok(NULL, " "));
            int owner = atoi(strtok(NULL, " "));
            int turns = atoi(strtok(NULL, " "));

            flights[num_flights]->from = from;
            flights[num_flights]->to = to;
            flights[num_flights]->ship_count = ship_count;
            flights[num_flights]->owner = owner;
            flights[num_flights]->turns = turns;

            num_flights++;
        }
        else if(strstr(line, "done")) {
            if(DEBUG) {
                fprintf(stderr, "Ok, time to make decisions!\n");
            }

            turns++;
            int total_commands_issued = 0;

            // CALCULATE THREATS AND STUFF
            for(int stari=0; stari<NUM_STARS;stari++) {
                Star *currstar = stars[stari];
                currstar->threat = 0;
                currstar->impending = 0;
                currstar->ongoing_attack = 0;
                currstar->links = 0;
                for(int j=0; j<currstar->num_neighbours; j++) {
                    Star *neighbour = currstar->neighbours[j];
                    if(neighbour->owner == -1) {
                    }
                    else if(neighbour->owner == 0) {
                    }
                    else if(neighbour->owner == 1) {
                    }
                    else if(neighbour->owner == 2) {
                        currstar->threat += neighbour->ship_count;
                    }
                }
            }

            // Add links
            for(int linki=0; linki<num_links; linki++) {
                stars[links[linki]->low_id]->links += 1;
                stars[links[linki]->high_id]->links += 1;
            }

            // MORE THREATS OH DEAR
            for(int flighti=0; flighti<num_flights; flighti++) {
                if(flights[flighti]->owner == 2) {
                    int stari = flights[flighti]->to;
                    stars[stari]->threat += flights[flighti]->ship_count;
                    stars[stari]->impending += flights[flighti]->ship_count;
                }
                // And our stuff too
                else {
                    int stari = flights[flighti]->to;
                    stars[stari]->ongoing_attack += flights[flighti]->ship_count;
                }
            }

            // HERE ARE ALL THE GOOD THINGS OK
            for(int stari=0; stari<NUM_STARS;stari++) {
                Star *currstar = stars[stari];
                if(currstar->owner==0) {
                    if(DEBUG) {
                        fprintf(stderr, "Ok, going through my star %d\n", currstar->id);
                    }

                    // Not much we can do...
                    if(currstar->ship_count <= 0) {
                        continue;
                    }

                    int flights_issued = 0;

                    // Categorize neighbour status
                    int num_own = 0;
                    int num_unc = 0;
                    int num_ene = 0;
                    int num_fri = 0;

                    int total_neighbour_threat = 0;
                    int total_allied_neighbours = 0;

                    for(int j=0; j<currstar->num_neighbours; j++) {
                        Star *neighbour = currstar->neighbours[j];
                        if(neighbour->owner == -1) {
                            if(DEBUG) {
                                fprintf(stderr, "Added unc\n");
                            }
                            unconquered[num_unc++] = neighbour;
                        }
                        else if(neighbour->owner == 0) {
                            if(DEBUG) {
                                fprintf(stderr, "Added own\n");
                            }
                            owned[num_own++] = neighbour;
                            total_neighbour_threat += neighbour->threat;
                            total_allied_neighbours++;
                        }
                        else if(neighbour->owner == 1) {
                            if(DEBUG) {
                                fprintf(stderr, "Added friend\n");
                            }
                            friends[num_fri++] = neighbour;
                            total_neighbour_threat += neighbour->threat;
                            total_allied_neighbours++;
                        }
                        else if(neighbour->owner == 2) {
                            if(DEBUG) {
                                fprintf(stderr, "Added enemy\n");
                            }
                            enemies[num_ene++] = neighbour;
                        }
                    }

                    int average_neighbour_threat = (int) ((float)total_neighbour_threat/(float)total_allied_neighbours);
                    if(total_allied_neighbours >= 3 && currstar->threat > average_neighbour_threat*2.0f) {
                        currstar->bastion = 1;
                    }
                    else if(currstar->threat < average_neighbour_threat*1.5f) {
                        currstar->bastion = 0;
                    }

                    int EXPLORE_MIN = 6;
                    int PASSIVE_DEFENSE = 11;

                    // Leave defense so enemy wastes ships whenever possible
                    int impending_reduction = currstar->impending - PASSIVE_DEFENSE + 1;
                    if(impending_reduction < 0) {
                        impending_reduction = 0;
                    }
                    int available_ships = currstar->ship_count - impending_reduction;
                    if(available_ships < 0) {
                        available_ships = 0;
                    }

                    if(DEBUG) {
                        fprintf(stderr, "Pre exploring... ships %d\n", available_ships);
                    }

                    // Explore new stars
                    while(available_ships >= EXPLORE_MIN && flights_issued < 3 && num_unc > 0
                          && total_commands_issued < MAX_NUM_COMMANDS) {
                        if(DEBUG) {
                            fprintf(stderr, "Exploring...\n");
                        }
                        int explore_with = EXPLORE_MIN;
                        int explore_index = -1;
                        int flight_time = 6;
                        for(int randexplore=0; randexplore<5; randexplore++) {
                            int try_explore = rand() % num_unc;
                            int try_flight_time = get_flight_distance(currstar, unconquered[try_explore]);
                            if(explore_index == -1 ||
                               unconquered[try_explore]->richness > unconquered[explore_index]->richness
                               || (unconquered[try_explore]->richness >= unconquered[explore_index]->richness
                               && try_flight_time < flight_time)) {
                                if(unconquered[try_explore]->impending > 0) {
                                    int evil_flight_time = 6;
                                    for(int findex=0; findex<num_flights; findex++) {
                                        if(flights[findex]->owner == 2 && flights[findex]->to == unconquered[try_explore]->id) {

                                            int maybe_smallest_eta = flights[findex]->turns;
                                            if(maybe_smallest_eta < evil_flight_time) {
                                                evil_flight_time = maybe_smallest_eta;
                                            }
                                        }
                                    }
                                    if(try_flight_time < evil_flight_time) {
                                        // Excellent! Let's steal it...
                                        flight_time = try_flight_time;
                                        explore_index = try_explore;
                                        break;
                                    }
                                    else if(try_flight_time == evil_flight_time) {
                                        // Should be OK to snag!
                                        if(available_ships > unconquered[try_explore]->impending) {
                                            explore_index = try_explore;
                                            flight_time = try_flight_time;
                                            int good_amount = unconquered[try_explore]->impending + 6;
                                            if(available_ships >= good_amount) {
                                                explore_with = good_amount;
                                            }
                                            else {
                                                explore_with = unconquered[try_explore]->impending-3;
                                            }
                                            break;
                                        }
                                        // Won't win it but at least make enemy waste maximum on empty planet defense
                                        else if(unconquered[try_explore]->ongoing_attack == 0
                                                && available_ships >= unconquered[try_explore]->impending-4) {
                                            explore_with = unconquered[try_explore]->impending-4;
                                            explore_index = try_explore;
                                            flight_time = try_flight_time;
                                        }
                                    }
                                }
                                else if(unconquered[try_explore]->ongoing_attack == 0) {
                                    // OK, good enough
                                    explore_index = try_explore;
                                    flight_time = try_flight_time;
                                }
                            }
                        }
                        if(explore_index != -1) {
                            if(explore_with == EXPLORE_MIN && available_ships > EXPLORE_MIN) {
                                explore_with += (rand() % (available_ships - EXPLORE_MIN));
                            }
                            available_ships -= explore_with;
                            flights_issued += 1;
                            flight_time = get_flight_distance(currstar, unconquered[explore_index]);
                            sprintf(commands[total_commands_issued++],
                                    "fly %d %d %d",
                                    currstar->id, unconquered[explore_index]->id, explore_with);

                            flights[num_flights]->from = currstar->id;
                            flights[num_flights]->to = unconquered[explore_index]->id;
                            flights[num_flights]->ship_count = explore_with;
                            flights[num_flights]->owner = 0;
                            flights[num_flights]->turns = flight_time;
                            currstar->ship_count -= explore_with;
                            num_flights++;
                            unconquered[explore_index]->ongoing_attack += explore_with;
                        }
                        else {
                            break;
                        }
                    }

                    int ATTACK_TRIES = 4;

                    // Attack evil bad stars
                    if(available_ships > 0 && flights_issued < 3 && total_commands_issued < MAX_NUM_COMMANDS
                       && num_ene > 0 && available_ships > 0) {
                        if(DEBUG) {
                            fprintf(stderr, "Attacking...\n");
                        }

                        // Just try a few times, flailing wildly
                        for(int i=0; i<ATTACK_TRIES; i++) {
                            Star *target = enemies[0];

                            // Add random greed
                            for(int greed=0; greed<3; greed++) {
                                Star *greedy_target = enemies[rand() % num_ene];
                                if(greedy_target->richness > target->richness) {
                                    target = greedy_target;
                                }
                            }
                            int defense = PASSIVE_DEFENSE + target->ship_count;

                            // Add spawning stuff to defense
                            int flight_time = get_flight_distance(currstar, target);
                            int target_prod = target->turns_to_prod;
                            while(flight_time >= target_prod) {
                                defense += target->richness;
                                target_prod += 5;
                            }

                            // Add flights to defense
                            for(int findex=0; findex<num_flights; findex++) {
                                if(flights[findex]->to == target->id) {
                                    if(flights[findex]->owner == 2 && flights[findex]->turns <= flight_time) {
                                        defense += flights[findex]->ship_count;
                                    }
                                    else if(flights[findex]->turns == flight_time) {
                                        defense -= flights[findex]->ship_count;
                                    }
                                }
                            }

                            // Might be wasteful to send too overwhelming attacks
                            if(defense < -20) {
                                // Do nothing
                            }
                            // Don't mount impossible attacks ...
                            else if(available_ships >= defense) {
                                // Also skip perfectly viable attacks every now and then, ok
                                if(rand() % 7 != 0) {
                                    int attack_ships = defense;
                                    int extra_ships = rand() % (available_ships - attack_ships + 1);
                                    attack_ships += extra_ships;
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, target->id, attack_ships);
                                    available_ships -= attack_ships;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = target->id;
                                    flights[num_flights]->ship_count = attack_ships;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    num_flights++;
                                    currstar->ship_count -= attack_ships;
                                    target->ongoing_attack += attack_ships;
                                }
                            }
                            // ... except for every now and then
                            else if(available_ships > 0 && (rand() % available_ships) > 11) {
                                int hopelessness = defense - available_ships;
                                hopelessness - target->richness; // Add hope by greed
                                if(hopelessness <= 0) {
                                    hopelessness = 3; // Always be a bit hopeless
                                }
                                if(hopelessness <= 9 && (rand() % hopelessness) == 0) {
                                    int attack_ships = available_ships;
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, target->id, attack_ships);
                                    available_ships -= attack_ships;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = target->id;
                                    flights[num_flights]->ship_count = attack_ships;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    num_flights++;
                                    currstar->ship_count -= attack_ships;
                                    target->ongoing_attack += attack_ships;
                                }
                            }
                        }
                    }

                    // Send critical defensive reinforcements
                    // With any luck it makes the enemy waste more on our static defense
                    if(flights_issued < 3 && available_ships > 0) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->owner == 0 || neighbour->owner == 1) {
                                if(neighbour->impending > neighbour->ship_count+PASSIVE_DEFENSE) {
                                    int neighbour_defense = neighbour->ship_count+PASSIVE_DEFENSE-1;
                                    int enemy_attack = 0;
                                    int enemy_eta = 6;
                                    // Investigate enemy ETA and strength
                                    for(int findex=0; findex<num_flights; findex++) {
                                        if(flights[findex]->owner == 2 && flights[findex]->to == neighbour->id) {
                                            int maybe_smallest_eta = flights[findex]->turns;
                                            if(maybe_smallest_eta < enemy_eta) {
                                                enemy_eta = maybe_smallest_eta;
                                            }
                                            enemy_attack += flights[findex]->ship_count;
                                        }
                                    }

                                    // Pointless if we can't arrive in time
                                    int our_travel_time = get_flight_distance(currstar, neighbour);
                                    if(our_travel_time <= enemy_eta) {
                                        // Investigate our defensive capabilities
                                        for(int findex=0; findex<num_flights; findex++) {
                                            if(flights[findex]->to == neighbour->id
                                               && (flights[findex]->owner == 0 || flights[findex]->owner == 1)
                                               && flights[findex]->turns <= enemy_eta) {
                                                neighbour_defense += flights[findex]->ship_count;
                                            }
                                        }

                                        // Let's also ignore this if we cannot easily do it with one reinforcement batch
                                        int reinforcements_needed = neighbour_defense - enemy_attack;
                                        if(reinforcements_needed > 0 && reinforcements_needed <= available_ships) {
                                            currstar->idle_turns = 0;
                                            int reinforcements = reinforcements_needed;
                                            if(reinforcements < available_ships) {
                                                reinforcements += (rand() % (available_ships-reinforcements));
                                            }
                                            sprintf(commands[total_commands_issued++],
                                                    "fly %d %d %d",
                                                    currstar->id, neighbour->id, reinforcements);
                                            available_ships -= reinforcements;
                                            flights_issued += 1;

                                            flights[num_flights]->from = currstar->id;
                                            flights[num_flights]->to = neighbour->id;
                                            flights[num_flights]->ship_count = reinforcements;
                                            flights[num_flights]->owner = 0;
                                            flights[num_flights]->turns = our_travel_time;
                                            currstar->ship_count -= reinforcements;
                                            num_flights++;
                                            neighbour->ongoing_attack += reinforcements;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Send reinforcements to bastion, if any
                    if(currstar->bastion == 0 && currstar->threat < average_neighbour_threat
                       && num_unc < 2 && available_ships > 0 && currstar->impending == 0) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->bastion && (neighbour->owner == 0 || neighbour->owner==1)) {
                                int flight_time = get_flight_distance(currstar, neighbour);
                                int enemy_flight_time = 6;
                                int reinforcements = available_ships;

                                // Find lowest enemy flight time that we have to respond to
                                for(int findex=0; findex<num_flights; findex++) {
                                    if(flights[findex]->to == neighbour->id
                                       && flights[findex]->owner == 2) {
                                        int maybe_lowest = flights[findex]->turns;
                                        if(maybe_lowest < enemy_flight_time) {
                                            enemy_flight_time = maybe_lowest;
                                        }
                                    }
                                }

                                int enemy_offense = 0;
                                int possible_defense = neighbour->ship_count + 10; // passive
                                for(int findex=0; findex<num_flights; findex++) {
                                    if(flights[findex]->to == neighbour->id
                                       && flights[findex]->turns <= enemy_flight_time) {
                                        if(flights[findex]->owner == 2) {
                                            enemy_offense += flights[findex]->ship_count;
                                        }
                                        else {
                                            possible_defense += flights[findex]->ship_count;
                                        }
                                        int target_prod = neighbour->turns_to_prod;
                                        while(flight_time >= target_prod) {
                                            possible_defense += neighbour->richness;
                                            target_prod += 5;
                                        }
                                    }
                                }

                                if(possible_defense < enemy_offense) {
                                    // We want to make sure that it is not pointless to send troops
                                    // (reinforcements not enough or not there in time)
                                    if(possible_defense + reinforcements < enemy_offense
                                       || flight_time > enemy_flight_time) {
                                       continue;
                                    }
                                }

                                sprintf(commands[total_commands_issued++], "fly %d %d %d",
                                        currstar->id, neighbour->id, reinforcements);
                                available_ships -= reinforcements;
                                flights_issued += 1;

                                flights[num_flights]->from = currstar->id;
                                flights[num_flights]->to = neighbour->id;
                                flights[num_flights]->ship_count = reinforcements;
                                flights[num_flights]->owner = 0;
                                flights[num_flights]->turns = flight_time;
                                currstar->ship_count -= reinforcements;
                                neighbour->ongoing_attack += reinforcements;
                                num_flights++;
                            }
                        }
                    }

                    // Send urgent but not hopeless reinforcements
                    if(currstar->threat < average_neighbour_threat
                       &&num_unc == 0 && currstar->idle_turns >= 4 && flights_issued < 3 && available_ships > 0) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->owner == 0 || neighbour->owner == 1) {
                                if(neighbour->impending == 0 && neighbour->threat > currstar->threat * 4) {
                                    currstar->idle_turns = 0;
                                    int reinforcements = 3*available_ships/4;
                                    if(reinforcements <= 0) {
                                        reinforcements = 1;
                                    }
                                    int flight_time = get_flight_distance(currstar, neighbour);
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, neighbour->id, reinforcements);
                                    available_ships -= reinforcements;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = neighbour->id;
                                    flights[num_flights]->ship_count = reinforcements;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    currstar->ship_count -= reinforcements;
                                    num_flights++;
                                    neighbour->ongoing_attack += reinforcements;
                                }
                            }
                        }
                    }

                    // Send needed but not hopeless reinforcements
                    if(currstar->threat < average_neighbour_threat
                       && num_unc == 0 && currstar->idle_turns >= 8 && flights_issued < 3 && available_ships > 5) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->owner == 0 || neighbour->owner == 1) {
                                if(neighbour->impending == 0 && neighbour->threat > currstar->threat * 1.5f) {
                                    currstar->idle_turns = 0;
                                    int reinforcements = available_ships/2;
                                    if(reinforcements <= 0) {
                                        reinforcements = 1;
                                    }
                                    int flight_time = get_flight_distance(currstar, neighbour);
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, neighbour->id, reinforcements);
                                    available_ships -= reinforcements;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = neighbour->id;
                                    flights[num_flights]->ship_count = reinforcements;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    currstar->ship_count -= reinforcements;
                                    num_flights++;
                                    neighbour->ongoing_attack += reinforcements;
                                }
                            }
                        }
                    }

                    // Make links
                    if(num_unc < 2 && currstar->idle_turns >= 4 && flights_issued < 3 && available_ships > 0) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->owner == 1) {
                                if(neighbour->links == 0) {
                                    int already_linking = 0;
                                    // Make sure we're not otw already
                                    for(int findex=0; findex<num_flights; findex++) {
                                        if(flights[findex]->to == neighbour->id
                                           && flights[findex]->from == currstar->id
                                           && flights[findex]->owner == 0) {
                                                already_linking = 1;
                                                break;
                                        }
                                    }
                                    if(already_linking) {
                                        continue;
                                    }
                                    neighbour->links += 1;
                                    currstar->idle_turns = 0;
                                    int link_crew = available_ships/8;
                                    if(link_crew <= 0) {
                                        link_crew = 1;
                                    }
                                    int flight_time = get_flight_distance(currstar, neighbour);
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, neighbour->id, link_crew);
                                    available_ships -= link_crew;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = neighbour->id;
                                    flights[num_flights]->ship_count = link_crew;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    neighbour->ongoing_attack += link_crew;
                                    num_flights++;
                                }
                            }
                        }
                    }

                    // Always send away forces if there is no threat or exploration
                    if(num_unc == 0 && num_ene == 0 && currstar->threat == 0) {
                        Star *some_enemy = NULL;
                        // Eh, just assume whichever known enemy is good to point at
                        for(int maybenemy=0; maybenemy < NUM_STARS; maybenemy++) {
                            if(stars[maybenemy]->owner == 2) {
                                some_enemy = stars[maybenemy];
                            }
                        }
                        if(some_enemy != NULL) {
                            float enemy_distance = get_float_distance(currstar, some_enemy);
                            for(int j=0; j<currstar->num_neighbours; j++) {
                                Star *neighbour = currstar->neighbours[j];
                                float neighbour_enemy_distance = get_float_distance(neighbour, some_enemy);
                                if(neighbour_enemy_distance < enemy_distance) {
                                    int reinforcements = available_ships;
                                    int flight_time = get_flight_distance(currstar, neighbour);
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, neighbour->id, reinforcements);
                                    available_ships -= reinforcements;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = neighbour->id;
                                    flights[num_flights]->ship_count = reinforcements;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    num_flights++;
                                    neighbour->ongoing_attack += reinforcements;
                                }
                            }
                        }
                    }

                    // Send reinforcements instead of doing nothing
                    if(currstar->threat <= average_neighbour_threat
                       && num_unc == 0 && currstar->idle_turns >= 12 && flights_issued < 3 && available_ships > 3) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->impending == 0 && (neighbour->owner == 0 || neighbour->owner == 1)) {
                                if(neighbour->threat > currstar->threat) {
                                    int back_and_forth = 0;
                                    // Avoid just sending back and forth...
                                    for(int findex=0; findex<num_flights; findex++) {
                                        if(flights[findex]->to == currstar->id
                                           && flights[findex]->from == neighbour->id) {
                                           back_and_forth = 1;
                                           break;
                                       }
                                    }
                                    if(back_and_forth) {
                                        continue;
                                    }
                                    currstar->idle_turns = 0;
                                    int reinforcements = available_ships/2;
                                    if(reinforcements <= 0) {
                                        reinforcements = 1;
                                    }
                                    int flight_time = get_flight_distance(currstar, neighbour);
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, neighbour->id, reinforcements);
                                    available_ships -= reinforcements;
                                    flights_issued += 1;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = neighbour->id;
                                    flights[num_flights]->ship_count = reinforcements;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    num_flights++;
                                    neighbour->ongoing_attack += reinforcements;
                                }
                            }
                        }
                    }

                    // Send reinforcements if very bloated
                    if(currstar->threat <= average_neighbour_threat
                       && num_unc == 0 && available_ships >= (turns+50)/10 && currstar->idle_turns >= 5 && currstar->impending == 0 && flights_issued < 3) {
                        for(int j=0; j<currstar->num_neighbours; j++) {
                            Star *neighbour = currstar->neighbours[j];
                            if(neighbour->owner == 0 || neighbour->owner == 1) {
                                if(neighbour->impending == 0 && neighbour->threat >= currstar->threat) {
                                    int back_and_forth = 0;
                                    // Avoid just sending back and forth...
                                    for(int findex=0; findex<num_flights; findex++) {
                                        if(flights[findex]->to == currstar->id
                                           && flights[findex]->from == neighbour->id) {
                                           back_and_forth = 1;
                                           break;
                                       }
                                    }
                                    if(back_and_forth) {
                                        continue;
                                    }
                                    flights_issued++;
                                    int reinforcements = available_ships/2;
                                    if(reinforcements <= 0) {
                                        reinforcements = 1;
                                    }
                                    int flight_time = get_flight_distance(currstar, neighbour);
                                    sprintf(commands[total_commands_issued++],
                                            "fly %d %d %d",
                                            currstar->id, neighbour->id, reinforcements);
                                    available_ships -= reinforcements;

                                    flights[num_flights]->from = currstar->id;
                                    flights[num_flights]->to = neighbour->id;
                                    flights[num_flights]->ship_count = reinforcements;
                                    flights[num_flights]->owner = 0;
                                    flights[num_flights]->turns = flight_time;
                                    num_flights++;
                                    neighbour->ongoing_attack += reinforcements;
                                }
                            }
                        }
                    }
                    if(flights_issued == 0) {
                        currstar->idle_turns += 1;
                    }
                    else {
                        currstar->idle_turns = 0;
                    }
                }

            }

            for(int i=0; i<total_commands_issued; i++) {
                printf("%s\n", commands[i]);
            }
            printf("done\n");
            num_links = 0;
            num_flights = 0;
        }
    }

    free(line);
    // leak everything else because IN TIMES OF WAR THERE IS NO CODE QUALITY
}
